package in.connect2tech.expressive_scenarios_02.features;

import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_TicTacToe_Compare_DataTable {

	private List<List<String>> board;

	@Given("^a board like this:$")
	public void a_board_like_this(DataTable arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		// throw new PendingException();
		board = arg1.raw();
	}

	@When("^player x plays in row (\\d+), column (\\d+)$")
	public void player_x_plays_in_row_column(int arg1, int arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// System.out.println(board);
		// throw new PendingException();
	}

	@Then("^the board should look like this:$")
	public void the_board_should_look_like_this(DataTable arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		// throw new PendingException();
		arg1.diff(board);
	}
}
