package in.connect2tech.rest.ch15;

import cucumber.api.java.en.*;

import java.io.PrintWriter;
import java.util.List;

import com.google.gson.Gson;

import cucumber.api.DataTable;
import cucumber.api.PendingException;

public class FruitSteps {
    
    /*@Given("^the system knows about the following fruit:$")
    public void theSystemKnowsAboutTheFollowingFruit(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }*/
    
    @Given("^the system knows about the following fruit:$")
    public void theSystemKnowsAboutTheFollowingFruit(List<Fruit> fruitList)
                                                            throws Throwable {
        Gson gson = new Gson();
        PrintWriter writer = new PrintWriter("fruit.json", "UTF-8");
        writer.println(gson.toJson(fruitList));
        writer.close();
        
        System.out.println("fruitList=========>"+fruitList);
        System.out.println("-----------------------------------------");
    }
}