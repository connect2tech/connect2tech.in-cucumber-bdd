package in.connect2tech.rest.ch15;

import cucumber.api.java.en.*;
import cucumber.api.PendingException;

public class RestSteps {
    
    @When("^the client requests GET /fruits$")
    public void theClientRequestsGETFruits() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the response should be JSON:$")
    public void theResponseShouldBeJSON(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }    
}