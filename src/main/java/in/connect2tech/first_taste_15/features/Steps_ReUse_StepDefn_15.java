package in.connect2tech.first_taste_15.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/*
^ - Beginning
$ - End
\"([^\"]*)\" - String
(\\d+) - Number
*/
public class Steps_ReUse_StepDefn_15 {
	@Given("^the price of a \"([^\"]*)\" is (\\d+)c$")
	public void the_price_of_a_is_c(String arg1, int arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
		System.out.println("the_price_of_a_is_c/arg1====>"+arg1);
		System.out.println("the_price_of_a_is_c/arg2====>"+arg2);
	}

	@When("^I checkout (\\d+) \"([^\"]*)\"$")
	public void i_checkout(int arg1, String arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
		System.out.println("i_checkout/arg1====>"+arg1);
		System.out.println("i_checkout/arg2====>"+arg2);

	}

	@Then("^the total price should be (\\d+)c$")
	public void the_total_price_should_be_c(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
		System.out.println("the_total_price_should_be_c/arg1====>"+arg1);
	}
}

