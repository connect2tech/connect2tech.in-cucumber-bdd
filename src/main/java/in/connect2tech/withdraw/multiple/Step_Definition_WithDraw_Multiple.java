package in.connect2tech.withdraw.multiple;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Definition_WithDraw_Multiple {
	@Given("^I have \\$(\\d+) in my account$")
	public void i_have_$_in_my_account(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@When("^I choose to withdraw the fixed amount of \\$(\\d+)$")
	public void i_choose_to_withdraw_the_fixed_amount_of_$(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^I should receive \\$(\\d+) cash$")
	public void i_should_receive_$_cash(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^the balance of my account should be \\$(\\d+)$")
	public void the_balance_of_my_account_should_be_$(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^I should see an error message$")
	public void i_should_see_an_error_message() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}
}
