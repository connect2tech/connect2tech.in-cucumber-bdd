package in.connect2tech.withdraw.multiple;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Definition_Password {
	@Given("^I try to create an account with password \"([^\"]*)\"$")
	public void i_try_to_create_an_account_with_password(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^I should see that the password is invalid$")
	public void i_should_see_that_the_password_is_invalid() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^I should see that the password is valid$")
	public void i_should_see_that_the_password_is_valid() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}
}
