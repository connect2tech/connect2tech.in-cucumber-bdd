package com.c2t.basic.examples;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_Examples {
	@Given("I have searched for items containing the word '(.*)'")
	public void buyerHasSearchedForWord(String keyword) {
	}

	@When("I filter results by type '(.*)'")
	public void filterResultsBy(String type) {
	}

	@Then("I should only see items containing '(.*)' of type '(.*)'")
	public void shouldSeeMatchingFilteredResults(String keyword, String type) {
	}
	
}
