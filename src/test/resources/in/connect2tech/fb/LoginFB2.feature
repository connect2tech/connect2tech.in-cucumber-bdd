Feature: Login Functionality 

Documentation

Scenario: Sucessful Login to Facebook 
	A user should be able to login to fb with valid user and password.
	
	Given User Is on Home Page of FaceBook 
	When The user resets existing username
	When The user resets existing password 
	When Enter valid username "user1"
	And Enter valid password "password1" 
	Then Login success message is displayed 
	
Scenario: UnSucessful Login to Facebook 
	A user should be able to login to fb with valid user and password.
	
	Given User Is on Home Page of FaceBook 
	When Enter invalid username "user1"
	And Enter invalid password "password1" 
	Then Login failed message is displayed 
		
Scenario: UnSucessful Login to Facebook with phone and password
	A user should be able to login to fb with valid user and password.
	
	Given User Is on Home Page of FaceBook 
	When Enter invalid username "user1"
	And Enter invalid password "password1" 
	Then Login failed message is displayed 
	
Scenario: UnSucessful Login to Facebook with invlaid phone and password
	A user should be able to login to fb with valid user and password.
	
	Given User Is on Home Page of FaceBook 
	When Enter invalid username "user1"
	And Enter invalid password "password1" 
	Then Login failed message is displayed 
