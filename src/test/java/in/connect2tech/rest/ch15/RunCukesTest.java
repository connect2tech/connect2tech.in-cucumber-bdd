package in.connect2tech.rest.ch15;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/in/connect2tech/rest/ch15/fruit_list.feature", glue = {
		"in.connect2tech.rest.ch15" }, plugin = { "pretty", "html:out" }, snippets = SnippetType.CAMELCASE)
public class RunCukesTest {
}
