package in.connect2tech.withdraw.multiple;

import org.junit.runner.RunWith;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/in/connect2tech/withdraw/multiple", glue = {
		"in.connect2tech.withdraw.multiple" })
public class Runner_WithDraw_Multiple {
}
