/***
 * Excerpted from "The Cucumber for Java Book",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/srjcuc for more book information.
***/
package in.connect2tech.step_definition_inside.ch07.concept19.nicebank;

import org.junit.Assert;

import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import in.connect2tech.step_definition_inside.ch07.concept19.support.KnowsTheDomain;
import in.connect2tech.step_definition_inside.ch07.concept19.transforms.MoneyConverter;

public class AccountSteps {
	KnowsTheDomain helper;

	public AccountSteps() {

	}

	public AccountSteps(KnowsTheDomain helper) {
		this.helper = helper;
	}

	@Given("^I have deposited (\\$\\d+\\.\\d+) in my account$")
	public void iHaveDeposited$InMyAccount(@Transform(MoneyConverter.class) Money amount) throws Throwable {
		System.out.println("helper==>" + helper);

		helper.getMyAccount().deposit(amount);

		Assert.assertEquals("Incorrect account balance -", amount, helper.getMyAccount().getBalance());
	}
}