package in.connect2tech.step_definition_inside.ch07;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/in/connect2tech/step_definition_inside/ch07/cash_withdrawal.feature", glue = {
		"in.connect2tech.step_definition_inside.ch07" }, plugin = "pretty", snippets = SnippetType.CAMELCASE)
public class RunCukesTest {
}
