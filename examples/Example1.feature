Feature: Examples Dynamic Values 


Scenario: Successful withdrawal from an account in credit 
	# the context
	Given I have $100 in my account 
	# the event(s)
	When I request $20 
	# the outcome(s)
	Then $20 should be dispensed