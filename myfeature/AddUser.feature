Feature: Add User

Scenario: Successful Add User
	Given Perform SetUp
	Given User is on Home Page
	When User Navigate To Add User Link
	And User enters Mandatory Fields
	And User Clicks On Add Button
	Then Message Displayed User added successfully