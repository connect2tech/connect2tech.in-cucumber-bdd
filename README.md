connect2tech.in-Cucumber-BDD
============================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.


# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request



# How to use the project

## Setting up the project locally
1. Take the project from share drive. It also contains the jar files in jar-files
2. Import the project in eclipse
3. Delete existing entries from .classpath file i.e delete entries of all the jar files.
4. Include the jar file in classpath from jar-files folder

**Cucumber BDD Auto Generated Code**
```
^ - Beginning
$ - End
\"([^\"]*)\" - String
(\\d+) - Number
```

## Examples

###### Example: decimal/floating/$100.00
- test/resources/in/connect2tech/ch07/feature1/cash_withdrawal.feature

###### Example: Background Example
- in/connect2tech/gherkin_basics/features/sample.feature
- in.connect2tech.gherkin_basics.features.Runner_Background.java

###### Example: Compare DataTable
- in.connect2tech.expressive_scenarios_02.features.Steps_TicTacToe_Compare_DataTable.java


###### Example
1. com.c2t.basic.fb.FacebookRunner is the runner file.
2. fb/LoginFB.feature is feature file
3. com.c2t.basic.fb.Steps_FaceBook is the steps file

###### Example: Using **String** and **Number** in Feature file
- in.connect2tech.first_taste_02.features

###### Example: Using **String** and **Number** in Feature file, with dynamic data **Examples**
- in.connect2tech.first_taste_13.features

###### Example: Re-using the step definition (i.e. same steps in more than 1 feature file. Common step definition for the steps)
- in.connect2tech.first_taste_15.features.Steps_ReUse_StepDefn_15.java


###### Example: Good Example
- in.connect2tech.withdraw.Step_Definition_WithDraw
```
withdraw_fixed_amount.feature
withdraw_fixed_amount_abstract.feature
```
